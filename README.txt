CONTENTS OF THIS FILE
=====================

 * Introduction
 * Features
 * Installation
 * Usage
 * API Usage
 * Author


INTRODUCTION
============
The slidesJsSlider module provides the ability to create a responsive slideshow
with features like touch and CSS3 transitions, based on jQuery slidesjs plugin.
Integrates Slidesjs jQuery plugin with Drupal 7(slidesJsSlider module).


FEATURES
========
  * Responsive (Create dynamic slideshows that adapt to any screen).
  * Touch (Swipe support that tracks touch movements on supported devices).
  * CSS3 transitions (Animations that run smoothly on modern devices).
  * Multiple slideshows
  * Playing and stopping slideshow


INSTALLATION
============

Dependencies
------------

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [jQuery Update](https://www.drupal.org/project/jquery_update)


To install slidesjs Slider Module:

  * Place this module directory in your modules folder (this will usually be
    "sites/all/modules/").
  * Enable the module within your Drupal site at Administer -> Site Building ->
    Modules (admin/build/modules).
  * Download the slidesjs library from http://slidesjs.com/
  * Unzip the file and rename the folder to "slidesjs"
    (pay attention to the case of the letters)
  * Put the folder in a libraries directory
    - Ex: sites/all/libraries
  * The following files are required 
    - jquery.slides.min.js
    - jquery.slides.js
  * Ensure you have a valid path similar to this one for all files
    - Ex: sites/all/libraries/slidesjs/sources/jquery.slides.min.js

USAGE
=====
The slidesJsSlider module is most commonly used with the Views module to turn
listings of images or other content into a carousel.

  * Install the Views module (http://drupal.org/project/views) on your Drupal
    site if you have not already.
  
  * Add a new view at Administration -> Structure -> Views
    (admin/structure/views).
  
  * Change the "Display format" of the view to "slidesJsSlider".
    Disable the "Use pager" option,
    which cannot be used with the slidesJsSlider style. Click the
    "Continue & Edit" button to configure the rest of the View.
  
  * Click on the "Settings" link next to the slidesJsSlider Format to configure the
    options for the carousel such as the animation speed and skin.
  
  * Add the items you would like to include into the carousel.
    Note that the preview of the carousel within Views probably
    will not appear correctly because the necessary CSS/JS
    is not loaded in the Viewsinterface. Save your view and
    visit a page URL containing the view to see how it appears.

API USAGE
=========
The slidesjs_slider_add function allows you to add the CSS/JS and required
slidesjs settings. The arguments are as follows:

  slidesjs_slider_add($class_name, $settings);

The $settings are the configuration options. The configuration options can be found at:
http://www.slidesjs.com/#docs

One other function 'slidesjs_slider_add()' allows you to passing a list of items that will be
in your carousel into theme('slidesjs_slider'). This can be add the CSS/JS and help to print
out the HTML list.

  print theme('slidesjs_slider', array('items' => $items, 'options' => $options, 'identifier' => $identifier));
Here the '$identifier' is use for differentiate the carousel selector.

For the more help, how to use the module see the admin/help/slidesjs_slider.


AUTHOR
======
Pitabas Behera(http://nettantra.com)
