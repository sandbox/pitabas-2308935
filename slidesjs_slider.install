<?php
/**
 * @file
 * Installation actions for FlexSlider.
 */

/**
 * Implements hook_schema().
 */
function slidesjs_slider_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time.
  $t = get_t();

  // Check to see if the slidesjs library is available.
  if($phase == 'runtime') {
   $requirements['slidesjs'] = array(
    'title' => $t('Slidesjs'),
    'description' => $t('Version 3.0.4 installed'),
    'severity' => REQUIREMENT_OK,
   );
   _slidesjs_slider_requirements_library_installed($requirements);
  }
  return $requirements;
}

/**
 * Check if the library is available.
 *
 * @param array $requirements.
 * Requirements definition.
 */
function _slidesjs_slider_requirements_library_installed(&$requirements) {
  $t = get_t();

  $path = libraries_get_path('slidesjs');
  $installed = file_exists($path . '/sources/jquery.slides.min.js') && file_exists($path . '/sources/jquery.slides.js');

  // Check the results of the test.
  if (!$installed) {
    $requirements['slidesjs']['description'] = $t('Slidesjs library not found. Please consult the README.txt for installation instructions.');
    $requirements['slidesjs']['severity'] = REQUIREMENT_ERROR;
    $requirements['slidesjs']['value'] = $t('Slidesjs library not found.');
    return;
  }
}
