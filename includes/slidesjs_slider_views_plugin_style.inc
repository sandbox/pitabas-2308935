<?php

/**
 * @file
 * Contains the slidesJsSlider style plugin.
 */

/**
 * Style plugin.
 *
 * @ingroup views_plugin_style
 */
class Slidesjs_slider_views_plugin_style extends views_plugin_style {

/**
 *
 * Set default options.
 * @$options
*/
  function option_definition() {
    $options = parent::option_definition();
    $options['default_css'] = array('default' => 1);
    $options['start'] = array('default' => '1');
    $options['navigation_active'] = array('default' => FALSE);
    $options['navigation_effect'] = array('default' => '');
    $options['pagination_active'] = array('default' => FALSE);
    $options['pagination_effect'] = array('default' => '');
    $options['width'] = array('default' => '');
    $options['height'] = array('default' => '');
    $options['play_active'] = array('default' => 0);
    $options['play_effect'] = array('default' => '');
    $options['play_interval'] = array('default' => '');
    $options['play_auto'] = array('default' => 0);
    $options['play_swap'] = array('default' => 1);
    $options['play_pause_on_hover'] = array('default' => 0);
    $options['play_restart_delay'] = array('default' => '');
    return $options;
  }

/**
 * Show a form to edit the style options.
*/
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Number of options to provide in count-based options.
    $slidesjs_slider_effects = array('slide' => 'Slide', 'fade' => 'Fade');
    $range = drupal_map_assoc(range(1, 10));

    $form['description'] = array(
      '#type' => 'markup',
      '#value' => '',
    );
    $form['default_css'] = array(
      '#type' => 'checkbox',
      '#title' => t('Load default CSS'),
      '#description' => t('[boolean] Load the default CSS.'),
      '#default_value' => isset($this->options['default_css'])? $this->options['default_css'] : '',
    );
    $form['start'] = array(
      '#type' => 'select',
      '#title' => t('Start position'),
      '#description' => t('The item that will be shown as the first item in the list upon loading.'),
      '#options' => $range,
      '#default_value' => $this->options['start'],
    );
    /* Width/Height Object */
    $form['width_height_object'] = array(
      '#type' => 'fieldset',
      '#title' => t('width (number) & height (number)'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );
    $form['width_height_object']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#size' => 10,
      '#maxlength' => 10,
      '#default_value' => $this->options['width'],
      '#description' => t('Put the slider width'),
    );
    $form['width_height_object']['height'] = array(
      '#type' => 'textfield',
      '#title' => t('Height'),
      '#size' => 10,
      '#maxlength' => 10,
      '#default_value' => $this->options['height'],
      '#description' => t('Put the slider height'),
    );
    /* Navigation Object */
    $form['navigation_object'] = array(
      '#type' => 'fieldset',
      '#title' => t('navigation (object)'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );
    $form['navigation_object']['navigation_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('navigation'),
      '#description' => t('[boolean] Generates next and previous buttons. You can set to false and use your own buttons. User defined buttons must have the following: previous button: class="slidesjs-previous slidesjs-navigation" next button: class="slidesjs-next slidesjs-navigation"'),
      '#default_value' => $this->options['navigation_active'],
    );
    $form['navigation_object']['navigation_effect'] = array(
      '#type' => 'select',
      '#title' => t('Play Effect'),
      '#options' => $slidesjs_slider_effects,
      '#default_value' => $this->options['navigation_effect'],
      '#description' => t('[string] Can be either "slide" or "fade".'),
    );
    /* Pagination Object */
    $form['pagination_object'] = array(
      '#type' => 'fieldset',
      '#title' => t('pagination (object)'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );
    $form['pagination_object']['pagination_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('pagination'),
      '#description' => t('[boolean] Create pagination items. You cannot use your own pagination. Sorry.'),
      '#default_value' => $this->options['pagination_active'],
    );
    $form['pagination_object']['pagination_effect'] = array(
      '#type' => 'select',
      '#title' => t('Play Effect'),
      '#options' => $slidesjs_slider_effects,
      '#default_value' => $this->options['pagination_effect'],
      '#description' => t('[string] Can be either "slide" or "fade".'),
    );
    /* Play Object */
    $form['playoption_object'] = array(
      '#type' => 'fieldset',
      '#title' => t('play (object)'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#parents' => array('style_options'),
    );
    $form['playoption_object']['play_active'] = array(
      '#type' => 'checkbox',
      '#title' => t('Play Active'),
      '#description' => t('[boolean] Generate the play and stop buttons.'),
      '#default_value' => $this->options['play_active'],
    );
    $form['playoption_object']['play_effect'] = array(
      '#type' => 'select',
      '#title' => t('Play Effect'),
      '#options' => $slidesjs_slider_effects,
      '#default_value' => $this->options['play_effect'],
      '#description' => t('[string] Can be either "slide" or "fade".'),
    );
    $form['playoption_object']['play_interval'] = array(
      '#type' => 'textfield',
      '#title' => t('Play Interval'),
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t('[number] Time spent on each slide in milliseconds.'),
      '#default_value' => $this->options['play_interval'],
    );
    $form['playoption_object']['play_auto'] = array(
      '#type' => 'checkbox',
      '#title' => t('Play Auto'),
      '#description' => t('[boolean] Start playing the slideshow on load.'),
      '#default_value' => $this->options['play_auto'],
    );
    $form['playoption_object']['play_swap'] = array(
      '#type' => 'checkbox',
      '#title' => t('Play Swap'),
      '#description' => t('[boolean] show/hide stop and play buttons'),
      '#default_value' => $this->options['play_swap'],
    );
    $form['playoption_object']['play_pause_on_hover'] = array(
      '#type' => 'checkbox',
      '#title' => t('Play pauseOnHover'),
      '#description' => t('[boolean] pause a playing slideshow on hover'),
      '#default_value' => $this->options['play_pause_on_hover'],
    );
    $form['playoption_object']['play_restart_delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Play restartDelay'),
      '#size' => 10,
      '#maxlength' => 10,
      '#description' => t('[number] restart delay on inactive slideshow'),
      '#default_value' => $this->options['play_restart_delay'],
    );
  }

/**
 *
 * Validate the errors.
 * The slidesJsSlider style cannot be used with a pager.
 * Disable the "Use pager" option for this display.
*/ 
  function validate() {
    $errors = parent::validate();
    if ($this->display->handler->use_pager()) {
      $errors[] = t('The slidesJsSlider style cannot be used with a pager. Disable the "Use pager" option for this display.');
    }
    return $errors;
  }
}
