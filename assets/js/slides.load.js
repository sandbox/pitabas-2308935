/*
 * Slidesjs Slider settings
*/

(function($) {
  Drupal.behaviors.slidesjs_slider = {
    attach: function (context, settings) {
      settings = settings || Drupal.settings;
      // If no carousels exist on this part of the page, work no further. 
      if (!settings.slidesjs_slider || !settings.slidesjs_slider.carousels) {
        return;
      }
      applyCarousel(settings);
    }
  };

  function applyCarousel(settings){
    settings =  Drupal.settings;
    $.each(settings.slidesjs_slider.carousels, function(key, options) {
      var carouselSelector = options.selector;
      $(carouselSelector).slidesjs({
        width: (options.width) ? options.width : 940,
        height: (options.height) ? options.height : 200,
        start : options.start,
        navigation: {
          active : (options.navigation_active) ? true : false,
          effect : (options.navigation_effect)? options.navigation_effect : "slide",
        },
        pagination: {
          active : (options.pagination_active) ? true : false,
          effect : (options.pagination_effect)? options.pagination_effect : "slide",
        },
        play : {
          active : (options.play_active) ? true : false,
          effect : (options.play_effect)? options.play_effect : "slide",
          interval: (options.play_interval)? options.play_interval : 5000,
          auto: (options.play_auto) ? true : false,
          swap: (options.play_swap) ? true : false,
          pauseOnHover: (options.play_pause_on_hover) ? true : false,
          restartDelay: (options.play_restart_delay) ? options.play_restart_delay : 2500,
        },
      });
    });
  }
})(jQuery);
