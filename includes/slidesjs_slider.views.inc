<?php

/**
 * @file
 * Views integration for slidesjsSlider module.
 */

/**
 * Implements hook_views_plugin().
 */
function slidesjs_slider_views_plugins() {
  return array(
    'style' => array(
      // Style plugin for the slidesjs_slider.
      'slidesjs_slider' => array(
        'title' => t('slidesjsSlider'),
        'help' => t('Display rows in a carousel via slidesjsSlider.'),
        'handler' => 'slidesjs_slider_views_plugin_style',
        'path' => drupal_get_path('module', 'slidesjs_slider') . '/includes',
        'theme' => 'slidesjs_slider_view',
        'theme path' => drupal_get_path('module', 'slidesjs_slider') . '/includes',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
    ),
  );
}

/**
 * Adds CSS/JS for Views based slideshow.
 */
function slidesjs_slider_views_add($view, $display_id = NULL) {
  static $dom_counter = 0;

  if (!isset($display_id)) {
    $display_id = empty($view->current_display) ? 'default' : $view->current_display;
  }

  $options = array();

  $options['view_options'] = array(
    'view_args' => check_plain(implode('/', $view->args)),
    'view_path' => check_plain($_GET['q']),
    'view_base_path' => $view->get_path(),
    'view_display_id' => $display_id,
    'view_name' => $view->name,
    'slidesjs_slider_dom_id' => isset($view->slidesjs_slider_dom_id) ? $view->slidesjs_slider_dom_id : ++$dom_counter,
  );

  foreach ($view->style_plugin->options as $key => $option) {
    if ($option) {
      $options[$key] = is_numeric($option) ? (int) $option : $option;
    }
  }

  $identifier = drupal_clean_css_identifier('slidesjs_slider_dom_' . $options['view_options']['slidesjs_slider_dom_id']);
  return slidesjs_slider_add($identifier, $options);
}

/**
 * Preprocess function for slidesjs-slider-view.tpl.php.
 */
function template_preprocess_slidesjs_slider_view(&$variables) {
  $view = $variables['view'];
  $display_id = empty($view->current_display) ? 'default' : $view->current_display;

  $attachments = slidesjs_slider_views_add($view, $display_id);
  foreach ($attachments['js'] as $data) {
    if (isset($data['type']) && $data['type'] == 'setting') {
      $settings = reset($data['data']['slidesjs_slider']['carousels']);
    }
  }

  $variables['slidesjs_slider_classes_array'] = array(
    'slidesjs-slider-carousel',
    drupal_clean_css_identifier('slidesjs-slider-view--' . $view->name . '--' . $display_id),
    drupal_clean_css_identifier('slidesjs-slider-dom-' . $settings['view_options']['slidesjs_slider_dom_id']),
  );
  $variables['slidesjs_slider_classes'] = implode(' ', $variables['slidesjs_slider_classes_array']);

  $pager_offset = isset($view->pager['offset']) ? $view->pager['offset'] : $view->offset;

  foreach ($variables['rows'] as $id => $row) {
    $number = $id + 1 + $pager_offset;
    $parity = ($number % 2 == 0) ? 'even' : 'odd';
    $variables['row_classes'][$id] = 'slidesjs-slider-item-' . $number . ' ' . $parity;
  }
}
